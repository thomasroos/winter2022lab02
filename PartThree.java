import java.util.Scanner;
public class PartThree {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Square Length: ");
		int length1 = scan.nextInt();
		System.out.println("Triangle Length: ");
		int length2 = scan.nextInt();
		System.out.println("Triangle Width: ");
		int width = scan.nextInt();
		
		System.out.println("Square Area :" + AreaComputations.areaSquare(length1));
		System.out.println("Triangle Area :" + AreaComputations.areaTriangle(length2, width));
	}
}