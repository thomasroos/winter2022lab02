public class MethodsTest {
  public static void main(String[] args) {
    int x = 10;
	System.out.println(x);
    methodNoInputNoReturn();
	System.out.println(x);
	
	methodOneInputNoReturn(10);
	methodOneInputNoReturn(x);
	methodOneInputNoReturn(x+50);
	
	methodTwoInputNoReturn(20,5.0);
	
	int x1 = methodNoInputReturnIn();
	System.out.println(x1);
	
	double x2 = sumSquareRoot(3,6);
	System.out.println(x2);
	
	String s1 = "Hello";
	String s2 = "Goodbye";
	
	System.out.println(s1.length());
	System.out.println(s2.length());
	
	System.out.println(SecondClass.addOne(50));
	SecondClass sc = new SecondClass();
	sc.addTwo(50);
	
  }
  
  public static void methodNoInputNoReturn() {
    System.out.println("I'm in a method that takes no input and returns nothing");
	int x = 50;
	System.out.println(x);
  }
  
  public static void methodOneInputNoReturn(int xyz) {
	System.out.println("Inside the method one input no return");
	System.out.println(xyz);
  }
  
  public static void methodTwoInputNoReturn(int xyz, double abc) {
	System.out.println(xyz);
	System.out.println(abc);
  }
  
  public static int methodNoInputReturnIn() {
	return 6;
  }
  
  public static double sumSquareRoot(int a, int b) {
	double c = a+b;
	c = Math.sqrt(c);
	return c;
  }
}