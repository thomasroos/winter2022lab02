public class AreaComputations {
	public static int areaSquare(int length) {
		int area = length*length;
		return area;
	}
	
	public static double areaTriangle(int length, int width) {
		double area = (length*width)/2.0;
		return area;
	}
}